import styled from 'styled-components/native';
import { Section } from './Section';
import { Fragment, useState } from 'react';
import { Entry } from './Entry';
import { StatusBar } from 'react-native';
import { EntryNew } from './EntryNew';
import { EntryCtxOverlay } from './EntryCtxOverlay';

type SectionType = 's';
type EntryType = 'd';

export type SectionData = {
  t: SectionType,
  id: string,
  title: string,
};

export type EntryData = {
  t: EntryType,
  id: string,
  text: string,

  force_into_edit?: boolean,
  cursor_backoffset?: number,
}


let initialData: (SectionData|EntryData)[] = [
  { "t": "s", "id": "sec_1", "title": "Section #1" },
  { "t": "d", "id": "rec_1", "text": "Entry 1 Long entry goes here so long it doesnt fit in one line yeah it sooo biig :flush:" },
  { "t": "d", "id": "rec_2", "text": "Entry 2" },
  { "t": "d", "id": "rec_3", "text": "Entry 3" },
  { "t": "s", "id": "sec_2", "title": "Section #2" },
  { "t": "d", "id": "rec_4", "text": "Entry 4" },
  { "t": "d", "id": "rec_5", "text": "Entry 5" },
  { "t": "d", "id": "rec_6", "text": "Entry 6" },
];

const Container = styled.ScrollView`
  width: 100%;
  height: 100%;
  flex-direction: column;
  overflow-y: scroll;
  background-color: #f7f7f7;
`;

const BottomSpacer = styled.View`
  height: 32px;
`;

function new_id(): string {
  return "" + Math.floor(Math.random() * 10000000);
}

function new_entry(): EntryData {
  return {
    "t": "d",
    "id": new_id(),
    "text": "",
  }
}

export default function App() {
  const [data, setData] = useState(initialData);
  const [entryCtxData, setEntryCtxData] = useState({
    "show": false,
    "x": 0,
    "y": 0,
    "w": 0,
    "h": 0,
    "e": undefined as undefined|EntryData
  });

  const insert_record = (idx: number, d: EntryData) => {
    let new_data = [...data];
    new_data.splice(idx + 1, 0, d);
    setData(new_data);
  };

  const update_record = (d: EntryData) => {
    let new_data = [...data];
    
    let idx = new_data.findIndex((e) => e.id == d.id );
    if (idx == -1) return;

    new_data[idx] = d;
    setData(new_data);
  };

  const delete_record = (d: SectionData|EntryData) => {
    let new_data = [...data];
    
    let idx = new_data.findIndex((e) => e.id == d.id );
    if (idx == -1) return;

    new_data.splice(idx, 1);
    setData(new_data);
  };

  const merge_record = (idx: number, newRec: EntryData) => {
    let new_data = [...data];

    let uidx = new_data.findIndex((e) => e.id == newRec.id );
    if (uidx == -1) return;
    
    new_data[uidx] = JSON.parse(JSON.stringify(newRec));
    new_data[uidx].id = new_id();

    new_data.splice(idx, 1);

    setData(new_data);
  };

  return <>
    <StatusBar/>
    <Container>
      {data.map((value, index) => {
        let entry;
        if (value["t"] === "s") {
          entry = <Section data={value as SectionData} />;
        } else {
          let v = value as EntryData;

          entry = <Entry 
            data={v}
            is_edit={v.force_into_edit}
            onRecordUpdate={ (d) => update_record(d) }
            onRecordRemove={ () => delete_record(v) }
            onNextEntryRq={ 
              (t) => {
                let e = { 
                  ...new_entry(), 
                  "text": t, 
                  "force_into_edit": true,
                  "cursor_backoffset": t.length
                };
                insert_record(index, e) 
              }
            }
            onMergeWithPrev={ 
              (append_text) => {
                let prev = data[index-1];
                
                if (prev.t == "d") {
                  let new_prev = { 
                    "t": "d" as EntryType, 
                    "id": prev.id, 
                    "text": prev.text + append_text, 
                    "force_into_edit": true,
                    "cursor_backoffset": append_text.length
                  };

                  merge_record(index, new_prev);
                }
              } 
            }
            onCtxReq={ (x,y,w,h) => {
              setEntryCtxData({
                "show": true,
                "x": x,
                "y": y,
                "w": w,
                "h": h,
                "e": v,
              });
            } }
          />;
        }

        let btn_new;
        if ((index == data.length - 1) || (data[index+1]["t"] === "s")) {
          btn_new = <EntryNew 
            onClick={ () => insert_record(index, { ...new_entry(), "force_into_edit": true }) }
          />;
        } else {
          btn_new = null;
        }

        return <Fragment key={"f" + value["id"]}>
          {entry}{btn_new}
        </Fragment>
      })}
      <BottomSpacer/>
    </Container>
    { entryCtxData.show
      ? <EntryCtxOverlay 
          data={entryCtxData} 
          onCtxHide={ ()=>setEntryCtxData({...entryCtxData, "show": false}) }
        />
      : <></> }
    </>;
}
