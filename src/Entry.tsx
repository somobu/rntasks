import { FC, useEffect, useRef, useState } from "react";
import { EntryData } from "./App";
import { styled } from "styled-components/native";
import { NativeSyntheticEvent, Text, TextInputKeyPressEventData } from "react-native";

type EntryProps = {
    is_edit?: boolean,
    data: EntryData,
    onNextEntryRq: (t: string) => void,
    onMergeWithPrev: (t: string) => void,
    onRecordUpdate: (t: EntryData) => void,
    onRecordRemove: () => void,
    onCtxReq: (x: number, y: number, w: number, h: number) => void
}

const Container = styled.View`
    min-height: 24px;
    margin-left: 16px;
    margin-right: 8px;
    flex-direction: column;
    justify-content: center;
`;

const Pressable = styled.Pressable`
    
`;

const StyledText = styled.Text`
    font-size: 16px;
    font-weight: 400;
    color: #777;
`;

const TextInput = styled.TextInput`
    margin: 0;
    padding: 0;

    font-size: 16px;
    font-weight: 400;
    color: #777;
    background-color: #eee;
`;

export const Entry: FC<EntryProps> = ({is_edit, data, onNextEntryRq, onMergeWithPrev, onRecordUpdate, onRecordRemove, onCtxReq}) => {
    const tRef = useRef(null as null|Text);
    const [text, setText] = useState(data.text);
    const [editMode, setEditMode] = useState(is_edit || false);
    const [selection, setSelection] = useState({start: 0, end: 0});

    const handleEditStart = () => {
        setText(data.text);
        setEditMode(true);
    }

    const handleEditDone = (text: string) => {
        setEditMode(false);

        data.cursor_backoffset = undefined;
        data.force_into_edit = undefined;
        data.text = text.trimEnd();

        if (data.text === "") {
            onRecordRemove();
        } else {
            onRecordUpdate(data);
        }
    };

    const handleSubmit = () => {
        let t = text.trimEnd();
        let s = t;

        let additional_text = "";
        if (selection.start != text.length-1) {
            s = t.substring(0, selection.start).trimEnd();
            additional_text = t.substring(selection.start);
        }

        handleEditDone(s);

        if (t != "") {
            onNextEntryRq(additional_text);
        }
    }

    const handleKeyPress = (e: NativeSyntheticEvent<TextInputKeyPressEventData>) => {
        if (e.nativeEvent.key == "Backspace") {
            if (selection.start == 0 && selection.end == 0) {
                onMergeWithPrev(text.trimEnd());
            }
        }
    };

    const handleTextChange = (t: string) => {
        let newlineIndex = t.indexOf("\n");
        if (newlineIndex >= 0) {
            let s = t.substring(0, newlineIndex).trimEnd();
            handleEditDone(s);

            let e = t.substring(newlineIndex + 1).split("\n");
            // TODO: spawn new entities
        } else {
            setText(t);
        }
    };


    // Jumping through hoops to set selection where we want it
    useEffect( () => {
        if (data.cursor_backoffset != undefined) {
            let cursor_offset = data.text.length - data.cursor_backoffset;
            setSelection({start: cursor_offset, end: cursor_offset});
        } else {
            setSelection({start: 0, end: 0});
        }
    }, [data])


    let contents;

    if (editMode) {
        contents = <TextInput
            value={ text }
            selection={ selection }
            autoFocus={ true }
            multiline={ false }
            enterKeyHint={ "next" } // Android cannot change key hint at runtime?!
            onKeyPress={ handleKeyPress }
            onChangeText={ handleTextChange }
            onSelectionChange={(e) => { setSelection(e.nativeEvent.selection)}}
            onSubmitEditing={ handleSubmit }
            onEndEditing={ () => handleEditDone(text) }
        />;
    } else {
        contents = <Pressable 
            onPress={ ()=>{(tRef.current as Text).measure((_fx, _fy, width, height, px, py) => {
                onCtxReq(px, py, width, height)
            })} }
            onLongPress={ ()=>{(tRef.current as Text).measure((_fx, _fy, width, height, px, py) => {
                onCtxReq(px, py, width, height)
            })} }
        >
            <StyledText ref={tRef}>{data.text}</StyledText>
        </Pressable>;
    }

    return <Container>{contents}</Container>;
}
