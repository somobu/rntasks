import { FC } from "react";
import { SectionData } from "./App";
import styled from "styled-components/native";

type SectionProps = {
    data: SectionData
}

const Container = styled.View`
    height: 48px;
    margin-top: 8px;
    margin-left: 8px;
    margin-right: 8px;
    padding-bottom: 4px;
    flex-direction: column;
    justify-content: flex-end;
`;

const Text = styled.Text`
    font-size: 24px;
    font-weight: 600;
    color: #777;
`;

const Hr = styled.View`
   width: 100%;
   height: 1px;
   margin-top: 2px;
   background-color: #888;
`;

export const Section: FC<SectionProps> = ({data}) => {
    return <Container>
        <Text>{data.title}</Text>
        <Hr/>
    </Container>;
}
