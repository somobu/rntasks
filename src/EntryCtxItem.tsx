import { FC } from "react";
import { styled } from "styled-components/native";


type Props = {
    children: any
};

const ItemContainer = styled.View`
    height: 40px;
    padding-left: 16px;
    padding-right: 32px;
    flex: 1;
    justify-content: center;
`;

const ItemText = styled.Text`
    font-size: 18px;
    font-weight: 500;
    color: #777;
`;

export const EntryCtxItem: FC<Props> = ({children})=> {
    return <ItemContainer>
        <ItemText>{children}</ItemText>
    </ItemContainer>
}
