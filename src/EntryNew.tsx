import { FC } from "react";
import { styled } from "styled-components/native";

const Container = styled.View`
    height: 24px;
    margin-left: 16px;
    margin-right: 8px;
    flex-direction: column;
    justify-content: center;
`;

const TextNew = styled.Text`
    font-size: 16px;
    font-weight: 400;
    color: #aaa;
`;

type EntryNewProps = {
    onClick: () => void
}

export const EntryNew: FC<EntryNewProps> = ({onClick}) => {
    return <Container>
        <TextNew onPress={ onClick }>
            {"Add new +"}
        </TextNew>
    </Container>;
};