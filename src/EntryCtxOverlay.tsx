import { FC, useState } from "react"
import { NativeSyntheticEvent, TextInputKeyPressEventData, useWindowDimensions } from "react-native";
import styled from "styled-components/native";
import { EntryCtxItem } from "./EntryCtxItem";
import { EntryData } from "./App";

type Props = {
    data: Data,
    onCtxHide: () => void,
};

type Data = {
    x: number,
    y: number,
    w: number,
    h: number,
    e?: EntryData,
}


const Blocker = styled.Pressable`
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    background-color: #00000022;
`;


const ItemList = styled.View`
    position: absolute;
    left: 16px;

    padding-top: 4px;
    padding-bottom: 4px;

    border-radius: 8px;
    background-color: #f9f9f9;
`;

const TextInput = styled.TextInput`
    position: absolute;

    left: 0;
    width: 100%;

    margin: 0;
    padding: 0;

    padding-left: 16px;
    padding-right: 8px;

    font-size: 16px;
    font-weight: 400;
    color: #777;
    background-color: #f7f7f7;
`;

export const EntryCtxOverlay: FC<Props> = ({data, onCtxHide}) => {
    const {height, width} = useWindowDimensions();
    const [text, setText] = useState(data.e?.text);
    const [selection, setSelection] = useState({start: 0, end: 0});

    const handleKeyPress = (e: NativeSyntheticEvent<TextInputKeyPressEventData>) => {
        if (e.nativeEvent.key == "Backspace") {
            if (selection.start == 0 && selection.end == 0) {
                // onMergeWithPrev(text.trimEnd());
            }
        }
    };

    const handleTextChange = (t: string) => {
        let newlineIndex = t.indexOf("\n");
        if (newlineIndex >= 0) {
            let s = t.substring(0, newlineIndex).trimEnd();
            // handleEditDone(s);

            let e = t.substring(newlineIndex + 1).split("\n");
            // TODO: spawn new entities
        } else {
            setText(t);
        }
    };

    return <>
        <Blocker onPress={onCtxHide}/>

        <TextInput
            style={{ top: data.y, minHeight: data.h }}
            value={ text }
            selection={ selection }
            autoFocus={ true }
            multiline={ true }
            onKeyPress={ handleKeyPress }
            onChangeText={ handleTextChange }
            onSelectionChange={(e) => { setSelection(e.nativeEvent.selection)}}
        />

        <ItemList style={{ top: data.y + data.h + 4 }}>
            <EntryCtxItem>Awoo</EntryCtxItem>
            <EntryCtxItem>Whoa</EntryCtxItem>
            <EntryCtxItem>Bere</EntryCtxItem>
            <EntryCtxItem>Bara</EntryCtxItem>
        </ItemList>
    </>;
}